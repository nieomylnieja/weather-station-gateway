package bmp280

import "fmt"

const (
	Name            = "BMP280"
	PressureUnit    = "Pa"
	TemperatureUnit = "C"
)

type Sensor struct {
	Temperature float64 `json:"temperature"`
	Pressure    float64 `json:"pressure"`
}

func (b *Sensor) String() string {
	return fmt.Sprintf("BMP280: {Temperature: %f [%s], Pressure: %f [%s]}", b.Temperature, TemperatureUnit, b.Pressure, PressureUnit)
}
