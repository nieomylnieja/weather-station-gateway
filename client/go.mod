module gitlab.com/nieomylnieja/weather-station-gateway

go 1.14

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/influxdata/influxdb-client-go v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
)
