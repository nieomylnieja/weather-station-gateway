package hm3301

import "fmt"

const (
	Name              = "HM3301"
	ConcentrationUnit = "ug/m3"
)

type Sensor struct {
	Standard    concentration `json:"standard"`
	Atmospheric concentration `json:"atmospheric"`
}

func (h *Sensor) String() string {
	return fmt.Sprintf("HM3301: {Standard: %s, Atmospheric: %s}", h.Standard.String(), h.Atmospheric.String())
}

type concentration struct {
	Pm1_0 float64 `json:"pm1_0"`
	Pm2_5 float64 `json:"pm2_5"`
	Pm10  float64 `json:"pm10"`
}

func (c *concentration) String() string {
	return fmt.Sprintf("PM1.0: %f [%s], PM2.5: %f [%s], PM10: %f [%s]",
		c.Pm1_0, ConcentrationUnit,
		c.Pm2_5, ConcentrationUnit,
		c.Pm10, ConcentrationUnit,
	)
}
