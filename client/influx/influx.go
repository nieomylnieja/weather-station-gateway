package influx

import (
	"context"
	"fmt"
	"time"

	influxdb "github.com/influxdata/influxdb-client-go"
	"github.com/influxdata/influxdb-client-go/api"
	"github.com/kelseyhightower/envconfig"
)

type Writer struct {
	client influxdb.Client
	api    api.WriteApiBlocking
}

func (w *Writer) Write(t time.Time, measurement, unit string, value float64, tags map[string]string) {
	p := influxdb.NewPointWithMeasurement(measurement).
		AddTag("unit", unit).
		AddField("value", value).
		SetTime(t)
	for k, v := range tags {
		p.AddTag(k, v)
	}
	if err := w.api.WritePoint(context.Background(), p); err != nil {
		fmt.Println(err.Error())
	}
}

func (w *Writer) Close() {
	w.client.Close()
}

func NewWriter() *Writer {
	var config struct {
		Username string `required:"true"`
		Password string `required:"true"`
		Url      string `required:"true"`
		Db       string `required:"true"`
	}
	envconfig.MustProcess("influx", &config)
	client := influxdb.NewClient(config.Url, fmt.Sprintf("%s:%s", config.Username, config.Password))
	if ready, err := client.Ready(context.Background()); !ready || err != nil {
		panic("Influx server is not ready!")
	}

	writeApi := client.WriteApiBlocking("", config.Db)
	return &Writer{
		client: client,
		api:    writeApi,
	}
}
