package main

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/eclipse/paho.mqtt.golang"

	"gitlab.com/nieomylnieja/weather-station-gateway/bmp280"
	"gitlab.com/nieomylnieja/weather-station-gateway/hm3301"
	"gitlab.com/nieomylnieja/weather-station-gateway/influx"
	"gitlab.com/nieomylnieja/weather-station-gateway/models"
)

const topic = "weatherStation"

type Writer interface {
	Write(t time.Time, measurement, unit string, value float64, tags map[string]string)
}

type Handler struct {
	Writer
}

func (w *Handler) messageHandler(_ mqtt.Client, msg mqtt.Message) {
	var dto models.Dto
	if err := json.Unmarshal(msg.Payload(), &dto); err != nil {
		fmt.Println("Failed to unmarshal msg")
	}
	fmt.Printf("BMP280:%s | HM3301: %s\n", dto.BMP280.String(), dto.HM3301.String())
	dto.Time = time.Now()
	w.Write(dto.Time, bmp280.Name, bmp280.TemperatureUnit, dto.BMP280.Temperature, map[string]string{"property": "temperature"})
	w.Write(dto.Time, bmp280.Name, bmp280.PressureUnit, dto.BMP280.Pressure, map[string]string{"property": "pressure"})

	hm3301Tags := map[string]string{"property": "particulate_matter", "type": "standard", "concentration": "PM1.0"}
	w.Write(dto.Time, hm3301.Name, hm3301.ConcentrationUnit, dto.HM3301.Standard.Pm1_0, hm3301Tags)
	hm3301Tags["concentration"] = "PM2.5"
	w.Write(dto.Time, hm3301.Name, hm3301.ConcentrationUnit, dto.HM3301.Standard.Pm2_5, hm3301Tags)
	hm3301Tags["concentration"] = "PM10"
	w.Write(dto.Time, hm3301.Name, hm3301.ConcentrationUnit, dto.HM3301.Standard.Pm10, hm3301Tags)
	hm3301Tags["type"] = "atmospheric"
	hm3301Tags["concentration"] = "PM1.0"
	w.Write(dto.Time, hm3301.Name, hm3301.ConcentrationUnit, dto.HM3301.Atmospheric.Pm1_0, hm3301Tags)
	hm3301Tags["concentration"] = "PM2.5"
	w.Write(dto.Time, hm3301.Name, hm3301.ConcentrationUnit, dto.HM3301.Atmospheric.Pm2_5, hm3301Tags)
	hm3301Tags["concentration"] = "PM10"
	w.Write(dto.Time, hm3301.Name, hm3301.ConcentrationUnit, dto.HM3301.Atmospheric.Pm10, hm3301Tags)
}

func main() {
	w := influx.NewWriter()
	h := &Handler{w}

	opts := mqtt.NewClientOptions().AddBroker(":1883")
	opts.SetClientID("gateway")
	opts.SetDefaultPublishHandler(h.messageHandler)

	c := mqtt.NewClient(opts)
	defer gracefulShutdown(c, w)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	if token := c.Subscribe(topic, 0, nil); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	select {}
}

func gracefulShutdown(c mqtt.Client, w *influx.Writer) {
	defer w.Close()
	if token := c.Unsubscribe(topic); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())
		os.Exit(1)
	}
	c.Disconnect(250)
}
