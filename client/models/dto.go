package models

import (
	"fmt"
	"time"

	"gitlab.com/nieomylnieja/weather-station-gateway/bmp280"
	"gitlab.com/nieomylnieja/weather-station-gateway/hm3301"
)

type Dto struct {
	Time   time.Time     `json:"time"`
	BMP280 bmp280.Sensor `json:"bmp280"`
	HM3301 hm3301.Sensor `json:"hm3301"`
}

func (d Dto) String() string {
	return fmt.Sprintf("Time: %s\n%s\n%s\n", d.Time.Format(time.RFC3339), d.BMP280.String(), d.HM3301.String())
}
